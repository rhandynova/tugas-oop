package i.com.hewan;

public class Ikan extends Hewan {
    private String habitat;


    public Ikan(int kaki, String alatNafas, String habitat) {
        super(kaki, alatNafas);
        this.habitat = habitat;
    }


    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getHabitat() {
        return habitat;
    }

    @Override
    public String tampil()
    {
        return "Kaki : "+kaki+"\n"+
            "Alat Nafas : "+alatNafas+"\n"+
                "Habitat : "+habitat;
    }

}
