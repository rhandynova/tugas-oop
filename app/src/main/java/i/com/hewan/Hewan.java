package i.com.hewan;

public class Hewan {

protected int kaki;
protected String alatNafas;

    public Hewan(int kaki, String alatNafas) {
        this.kaki = kaki;
        this.alatNafas = alatNafas;
    }

    public void setKaki(int kaki) {
        this.kaki = kaki;
    }

    public void setAlatNafas(String alatNafas) {
        this.alatNafas = alatNafas;
    }

    public int getKaki() {
        return kaki;
    }

    public String getAlatNafas() {
        return alatNafas;
    }

    public String tampil()
    {
        return "Kaki : "+kaki+"\n"+
                "Alat Nafas : "+alatNafas;
    }

}

